package ru.pyshinskiy.tm.bootstrap;

import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.Exception_Exception;
import ru.pyshinskiy.tm.api.endpoint.ISessionEndpoint;
import ru.pyshinskiy.tm.api.endpoint.SessionDTO;
import ru.pyshinskiy.tm.service.SessionService;

import javax.inject.Inject;

public final class SessionUpdater extends Thread {

    @Inject
    private ISessionEndpoint sessionEndpoint;

    @Inject
    private SessionService sessionService;

    @Override
    public void run() {
        while(true) {
            if(sessionService.getSessionDTO() != null) {
                try {
                    Thread.sleep(150000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                @Nullable SessionDTO updatedSession = null;
                try {
                    updatedSession = sessionEndpoint.updateSession(sessionService.getSessionDTO());
                    sessionService.setSessionDTO(updatedSession);
                    System.out.println("[Session update]");
                }
                catch (Exception_Exception e) {
                    System.out.println("Updating session is failed");
                }
            }
        }
    }
}
