package ru.pyshinskiy.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;
import ru.pyshinskiy.tm.api.endpoint.UserDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import javax.inject.Inject;

public final class UserEditCommand extends AbstractCommand {

    @Inject
    private IUserEndpoint userEndpoint;

    @Override
    @NotNull
    public String command() {
        return "user_edit";
    }

    @Override
    @NotNull
    public String description() {
        return "edit existing user";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final String userId = sessionService.getSessionDTO().getUserId();
        @NotNull final String currentUserPassword = userEndpoint.findOneUser(sessionService.getSessionDTO(), userId).getPasswordHash();
        System.out.println("[USER EDIT]");
        System.out.println("ENTER NEW USERNAME");
        @NotNull final UserDTO user = new UserDTO();
        user.setId(sessionService.getSessionDTO().getUserId());
        user.setLogin(terminalService.nextLine());
        System.out.println("ENTER NEW PASSWORD");
        user.setPasswordHash(terminalService.nextLine());
        userEndpoint.mergeUser(sessionService.getSessionDTO(), user);
        System.out.println("[OK]");
    }
}
