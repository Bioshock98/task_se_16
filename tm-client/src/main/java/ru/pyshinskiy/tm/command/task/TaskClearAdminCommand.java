package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.command.AbstractCommand;

import javax.inject.Inject;

public final class TaskClearAdminCommand extends AbstractCommand {

    @Inject
    private ITaskEndpoint taskEndpoint;

    @Override
    public boolean isAllowed() {
        if(sessionService.getSessionDTO() == null) return false;
        return sessionService.getSessionDTO().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "task_clear_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "clear all users tasks";
    }

    @Override
    public void execute() throws Exception {
        taskEndpoint.removeAllTasks(sessionService.getSessionDTO());
    }
}
