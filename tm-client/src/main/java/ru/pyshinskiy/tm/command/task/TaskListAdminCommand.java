package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.api.endpoint.SessionDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import javax.inject.Inject;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskListAdminCommand extends AbstractCommand {

    @Inject
    private ITaskEndpoint taskEndpoint;

    @Override
    public boolean isAllowed() {
        if(sessionService.getSessionDTO() == null) return false;
        return sessionService.getSessionDTO().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "task_list_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "list all users tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST ADMIN]");
        System.out.println("CHOOSE SORT TYPE");
        System.out.println("[createTime, startDate, endDate, status]");
        @NotNull final String option = terminalService.nextLine();
        @NotNull final SessionDTO session = sessionService.getSessionDTO();
        switch (option) {
            case "createTime" :
                printTasks(taskEndpoint.sortTasksByCreateTime(sessionService.getSessionDTO(), session.getUserId(), 1));
                break;
            case "startDate" :
                printTasks(taskEndpoint.sortTasksByStartDate(sessionService.getSessionDTO(), session.getUserId(), 1));
                break;
            case "finishDate" :
                printTasks(taskEndpoint.sortTasksByFinishDate(sessionService.getSessionDTO(), session.getUserId(), 1));
                break;
            case "status" :
                printTasks(taskEndpoint.sortTasksByStatus(sessionService.getSessionDTO(), session.getUserId(), 1));
        }
        System.out.println("[OK]");
    }
}
