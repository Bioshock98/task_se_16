package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.TaskDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import javax.inject.Inject;
import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskUnattachCommand extends AbstractCommand {

    @Inject
    private ITaskEndpoint taskEndpoint;

    @Override
    @NotNull
    public String command() {
        return "task_unattach";
    }

    @Override
    @NotNull
    public String description() {
        return "unattach task from project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UNATTACH TASK]");
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findAllTasksByUserId(sessionService.getSessionDTO());
        printTasks(tasks);
        final int taskNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String taskId = tasks.get(taskNumber).getId();
        @Nullable final TaskDTO task = taskEndpoint.findOneTaskByUserId(sessionService.getSessionDTO(), taskId);
        if(task == null) {
            throw new Exception("task doesn't exist");
        }
        task.setProjectId(null);
        taskEndpoint.mergeTask(sessionService.getSessionDTO(), task);
        System.out.println("[OK]");
    }
}
