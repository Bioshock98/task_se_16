package ru.pyshinskiy.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.ISessionEndpoint;
import ru.pyshinskiy.tm.api.endpoint.SessionDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import javax.inject.Inject;

public final class UserLogOutCommand extends AbstractCommand {

    @Inject
    private ISessionEndpoint sessionEndpoint;

    @Override
    @NotNull
    public String command() {
        return "user_log_out";
    }

    @Override
    @NotNull
    public String description() {
        return "log out";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("USER LOG OUT");
        @Nullable final SessionDTO currentSession = sessionService.getSessionDTO();
        sessionEndpoint.removeSession(currentSession.getUserId(), currentSession.getId());
        sessionService.setSessionDTO(null);
        System.out.println("[OK]");
    }
}
