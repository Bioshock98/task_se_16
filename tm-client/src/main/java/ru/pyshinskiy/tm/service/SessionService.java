package ru.pyshinskiy.tm.service;

import ru.pyshinskiy.tm.api.endpoint.SessionDTO;

public class SessionService {

    private SessionDTO sessionDTO;

    public SessionDTO getSessionDTO() {
        return sessionDTO;
    }

    public void setSessionDTO(SessionDTO sessionDTO) {
        this.sessionDTO = sessionDTO;
    }
}
