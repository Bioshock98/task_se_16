package ru.pyshinskiy.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.pyshinskiy.tm.api.endpoint.*;
import ru.pyshinskiy.tm.endpoint.ProjectEndpointService;
import ru.pyshinskiy.tm.endpoint.SessionEndpointService;

import java.lang.Exception;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateFromString;
import static ru.pyshinskiy.tm.util.date.DateUtil.toXMLGregorianCalendar;

@Category(ru.pyshinskiy.tm.ProjectEndpointTestIntegrate.class)
public class ProjectEndpointTest extends Assert {

    @Nullable
    private ISessionEndpoint sessionEndpoint;

    @Nullable
    private IProjectEndpoint projectEndpoint;

    @Nullable
    private SessionDTO sessionDTOUser;

    @Nullable
    private SessionDTO sessionDTOAdmin;

    @Before
    public void setUp() throws Exception {
        sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();
        projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();
        sessionDTOUser = sessionEndpoint.createSession("test", "1234");
        sessionDTOAdmin = sessionEndpoint.createSession("admin", "1234");
    }

    @After
    public void tearDown() throws Exception {
        projectEndpoint.removeAllProjectsByUserId(sessionDTOAdmin);
        projectEndpoint.removeAllProjectsByUserId(sessionDTOUser);
        sessionEndpoint.removeSession(sessionDTOUser.getUserId(), sessionDTOUser.getId());
        sessionEndpoint.removeSession(sessionDTOAdmin.getUserId(), sessionDTOAdmin.getId());
    }

    @Test
    public void findOneProjectByUserId() throws Exception {
        @NotNull final ProjectDTO projectDTO = createProjectDTO();
        projectEndpoint.persistProject(sessionDTOUser, projectDTO);
        assertEquals(projectDTO.getId(), projectEndpoint.findOneProjectByUserId(sessionDTOUser, projectDTO.getId()).getId());
    }

    @Test
    public void findAllProjectsByUserId() throws Exception {
        @NotNull final List<ProjectDTO> userProjects = new ArrayList<>();
        for(int i = 0; i < 5; i++) {
            @NotNull final ProjectDTO projectDTO = createProjectDTO();
            userProjects.add(projectDTO);
            projectEndpoint.persistProject(sessionDTOUser, projectDTO);
        }
        assertEquals(userProjects.size(), projectEndpoint.findAllProjectsByUserId(sessionDTOUser).size());
    }

    @Test
    public void persistProject() throws Exception {
        @NotNull final ProjectDTO projectDTO = createProjectDTO();
        projectEndpoint.persistProject(sessionDTOUser, projectDTO);
        assertEquals(projectDTO.getId(), projectEndpoint.findOneProjectByUserId(sessionDTOUser, projectDTO.getId()).getId());
    }

    @Test
    public void mergeProject() throws Exception {
        @NotNull final ProjectDTO projectDTO = createProjectDTO();
        projectDTO.setName("MERGED PROJECT TEST 1");
        projectEndpoint.mergeProject(sessionDTOUser, projectDTO);
        assertEquals(projectDTO.getName(), projectEndpoint.findOneProjectByUserId(sessionDTOUser, projectDTO.getId()).getName());
    }

    @Test
    public void removeProjectByUserId() throws Exception {
        @NotNull final ProjectDTO projectDTO = createProjectDTO();
        projectEndpoint.removeProjectByUserId(sessionDTOUser, projectDTO.getId());
        assertNull(projectEndpoint.findOneProjectByUserId(sessionDTOUser, projectDTO.getId()));
    }

    @Test
    public void removeAllProjectsByUserId() throws Exception {
        for(int i = 0; i < 5; i++) {
            projectEndpoint.persistProject(sessionDTOUser, createProjectDTO());
        }
        projectEndpoint.removeAllProjectsByUserId(sessionDTOUser);
        assertTrue(projectEndpoint.findAllProjectsByUserId(sessionDTOUser).isEmpty());
    }

    @Test
    public void findProjectByName() throws Exception {
        @NotNull final ProjectDTO projectDTO = createProjectDTO();
        projectEndpoint.persistProject(sessionDTOUser, projectDTO);
        assertEquals(projectDTO.getName(), projectEndpoint.findProjectByName(sessionDTOUser, projectDTO.getName()).get(0).getName());
    }

    @Test
    public void findProjectByDescription() throws Exception {
        @NotNull final ProjectDTO projectDTO = createProjectDTO();
        projectDTO.setDescription("FIND BY DESCRIPTION");
        projectEndpoint.persistProject(sessionDTOUser, projectDTO);
        assertEquals(projectDTO.getDescription(), projectEndpoint.findProjectByDescription(sessionDTOUser, projectDTO.getDescription()).get(0).getDescription());
    }

    @Test
    public void findOneProject() throws Exception {
        @NotNull final ProjectDTO projectDTO = createProjectDTO();
        projectEndpoint.persistProject(sessionDTOAdmin, projectDTO);
        assertEquals(projectDTO.getId(), projectEndpoint.findOneProject(sessionDTOAdmin, projectDTO.getId()).getId());
    }

    /*@Test
    public void findAllProjects() throws Exception {
        projectEndpoint.removeAllProjectsByUserId(sessionDTOAdmin);
        projectEndpoint.removeAllProjectsByUserId(sessionDTOUser);
        @NotNull final List<ProjectDTO> allProjects = new ArrayList<>();
        for(int i = 0; i < 5; i++) {
            @NotNull final ProjectDTO projectDTO = createProjectDTO();
            allProjects.add(projectDTO);
            projectEndpoint.persistProject(sessionDTOUser, projectDTO);
        }
        for(int i = 0; i < 5; i++) {
            @NotNull final ProjectDTO projectDTO = createProjectDTO();
            allProjects.add(projectDTO);
            projectEndpoint.persistProject(sessionDTOAdmin, projectDTO);
        }
        assertEquals(allProjects.size(), projectEndpoint.findAllProjects(sessionDTOUser).size());
    }*/

    @Test
    public void removeProject() throws Exception {
        @NotNull final ProjectDTO projectDTO = createProjectDTO();
        projectEndpoint.persistProject(sessionDTOAdmin, projectDTO);
        projectEndpoint.removeProject(sessionDTOAdmin, projectDTO.getId());
        assertNull(projectEndpoint.findOneProject(sessionDTOAdmin, projectDTO.getId()));
    }

    @NotNull
    private ProjectDTO createProjectDTO() throws Exception{
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setUserId(sessionDTOUser.getUserId());
        projectDTO.setName("PROJECT TEST " + new Random().toString());
        projectDTO.setDescription("TEST DESCRIPTION 2");
        projectDTO.setStatus(Status.IN_PROGRESS);
        projectDTO.setStartDate(toXMLGregorianCalendar(parseDateFromString("11.12.2019")));
        projectDTO.setFinishDate(toXMLGregorianCalendar(parseDateFromString("12.12.2019")));
        return projectDTO;
    }
}
