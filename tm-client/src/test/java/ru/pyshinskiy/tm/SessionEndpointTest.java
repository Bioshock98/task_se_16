package ru.pyshinskiy.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.pyshinskiy.tm.api.endpoint.ISessionEndpoint;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;
import ru.pyshinskiy.tm.api.endpoint.SessionDTO;
import ru.pyshinskiy.tm.api.endpoint.UserDTO;
import ru.pyshinskiy.tm.endpoint.SessionEndpointService;
import ru.pyshinskiy.tm.endpoint.UserEndpointService;

@Category(ru.pyshinskiy.tm.SessionEndpointTestIntegrate.class)
public class SessionEndpointTest extends Assert {

    @Nullable
    private ISessionEndpoint sessionEndpoint;

    @Nullable
    private IUserEndpoint userEndpoint;

    @Before
    public void setUp() {
        sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();
        userEndpoint = new UserEndpointService().getUserEndpointPort();
    }

    @After
    public void tearDown() throws Exception {
        @NotNull final SessionDTO sessionDTO = sessionEndpoint.createSession("test", "1234");
        @NotNull final SessionDTO sessionDTOAdmin = sessionEndpoint.createSession("admin", "1234");
        sessionEndpoint.removeAllSessionsByUserId(sessionDTO.getUserId());
        sessionEndpoint.removeAllSessionsByUserId(sessionDTOAdmin.getUserId());
    }

    @Test
    public void createSession() throws Exception {
        @NotNull final SessionDTO sessionDTO = sessionEndpoint.createSession("test", "1234");
        assertEquals(sessionDTO.getId(), sessionEndpoint.findSession(sessionDTO.getId()).getId());
    }

    @Test
    public void updateSession() throws Exception {
        @NotNull final SessionDTO sessionDTO = sessionEndpoint.createSession("admin", "1234");
        assertNotEquals(sessionDTO.getTimestamp(), sessionEndpoint.findSession(sessionDTO.getId()));
    }

    @Test
    public void removeSession() throws Exception {
        @Nullable final SessionDTO sessionDTO = sessionEndpoint.createSession("test", "1234");
        @Nullable final UserDTO userDTO = userEndpoint.getUserByLoginE(sessionDTO, "test");
        sessionEndpoint.removeSession(userDTO.getId(), sessionDTO.getId());
        assertNull(sessionEndpoint.findSession(sessionDTO.getId()));
    }
}
