package ru.pyshinskiy.tm.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.Repository;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.entity.User;

@Repository
public interface IUserRepository extends EntityRepository<User, String> {

    @Query("delete u from User u")
    void removeAll() throws Exception;

    @Query("select u from User u where u.login = :login")
    User getUserByLogin(@QueryParam("login") @NotNull final String login) throws Exception;
}
