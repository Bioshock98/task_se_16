package ru.pyshinskiy.tm.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.Repository;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.entity.Session;

import java.util.List;

@Repository
public interface ISessionRepository extends EntityRepository<Session, String> {

    @Query("select s from Session s where s.user.id = :userId")
    List<Session> findAllByUserId(@QueryParam("userId") String userId);

    @Query("delete s from Session s")
    void removeAll() throws Exception;

    @Query("select s from Session s where s.id = :id and s.user.id = :userId")
    Session findOneByUserId(@QueryParam("userId") @NotNull final String userId, @QueryParam("id") @NotNull final String id) throws Exception;

    @Query("delete s from Session s where s.user.id = :userId and s.id = :id")
    void removeByUserId(@QueryParam("userId") @NotNull final String userId, @QueryParam("id") @NotNull final String id) throws Exception;

    @Query("delete s from Session s where s.user.id = :userId")
    void removeAllByUserId(@QueryParam("userId") @NotNull final String userId) throws Exception;
}
