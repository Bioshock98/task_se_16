package ru.pyshinskiy.tm.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.Repository;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.entity.Task;

import java.util.List;

@Repository
public interface ITaskRepository extends EntityRepository<Task, String> {

    @Query("select t from Task t where t.user.id = :userId")
    List<Task> findAllByUserId(@QueryParam("userId") String userId);

    @Query("select t from Task t where t.user.id = :userId and t.project.id = :projectId")
    List<Task> findAllByProjectId(@QueryParam("userId") @NotNull final String userId, @QueryParam("projectId") @NotNull final String projectId) throws Exception;

    @Query("delete t from Task t")
    void removeAll() throws Exception;

    @Query("select t from Task t where t.id = :id and t.user.id = :userId")
    Task findOneByUserId(@QueryParam("userId") @NotNull final String userId, @QueryParam("id") @NotNull final String id) throws Exception;

    @Query("select t from Task t where t:user.id = :userId and t.name = :name")
    List<Task> findByName(@QueryParam("userId") @NotNull final String userId, @QueryParam("name") @NotNull final String name);

    @Query("select t from Task t where t:user.id = :userId and t.description = :description")
    List<Task> findByDescription(@QueryParam("userId") @NotNull final String userId, @QueryParam("description") @NotNull final String description);

    @Query("delete t from Task t where t.user.id = :userId and t.id = :id")
    void removeByUserId(@QueryParam("userId") @NotNull final String userId, @QueryParam("id") @NotNull final String id) throws Exception;

    @Query("delete t from Task t where t.user.id = :userId")
    void removeAllByUserId(@QueryParam("userId") @NotNull final String userId) throws Exception;

    @Query("select t from Task t where t.user.id= :userId order by t.createTime")
    List<Task> sortByCreateTime(@QueryParam("userId") @NotNull final String userId) throws Exception;

    @Query("select t from Task t where t.user.id= :userId order by t.startDate")
    List<Task> sortByStartDate(@QueryParam("userId") @NotNull final String userId) throws Exception;

    @Query("select t from Task t where t.user.id= :userId order by t.finishDate")
    List<Task> sortByFinishDate(@QueryParam("userId") @NotNull final String userId) throws Exception;

    @Query("select t from Task t where t.user.id= :userId order by t.status")
    List<Task> sortByStatus(@QueryParam("userId") @NotNull final String userId) throws Exception;
}
