package ru.pyshinskiy.tm.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.Repository;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.entity.Project;

import java.util.List;

@Repository
public interface IProjectRepository extends EntityRepository<Project, String> {

    @Query("select p from Project p where p.user.id = :userId")
    List<Project> findAllByUserId(@QueryParam("userId") String userId);

    @Query("delete p from Project p")
    void removeAll() throws Exception;

    @Query("select p from Project p where p.id = :id and p.user.id = :userId")
    Project findOneByUserId(@QueryParam("userId") @NotNull final String userId, @QueryParam("id") @NotNull final String id) throws Exception;

    @Query("select p from Project p where p:user.id = :userId and p.name = :name")
    List<Project> findByName(@QueryParam("userId") @NotNull final String userId, @QueryParam("name") @NotNull final String name);

    @Query("select p from Project p where p:user.id = :userId and p.description = :description")
    List<Project> findByDescription(@QueryParam("userId") @NotNull final String userId, @QueryParam("description") @NotNull final String description);

    @Query("delete p from Project p where p.user.id = :userId and p.id = :id")
    void removeByUserId(@QueryParam("userId") @NotNull final String userId, @QueryParam("id") @NotNull final String id) throws Exception;

    @Query("delete p from Project p where p.user.id = :userId")
    void removeAllByUserId(@QueryParam("userId") @NotNull final String userId) throws Exception;

    @Query("select p from Project p where p.user.id= :userId order by p.createTime")
    List<Project> sortByCreateTime(@QueryParam("userId") @NotNull final String userId) throws Exception;

    @Query("select p from Project p where p.user.id= :userId order by p.startDate")
    List<Project> sortByStartDate(@QueryParam("userId") @NotNull final String userId) throws Exception;

    @Query("select p from Project p where p.user.id= :userId order by p.finishDate")
    List<Project> sortByFinishDate(@QueryParam("userId") @NotNull final String userId) throws Exception;

    @Query("select p from Project p where p.user.id= :userId order by p.status")
    List<Project> sortByStatus(@QueryParam("userId") @NotNull final String userId) throws Exception;
}
