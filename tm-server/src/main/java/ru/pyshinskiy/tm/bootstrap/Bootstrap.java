package ru.pyshinskiy.tm.bootstrap;

import lombok.NoArgsConstructor;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ISessionEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;

import javax.inject.Inject;
import javax.xml.ws.Endpoint;

@NoArgsConstructor
public class Bootstrap {

    @Inject
    private SessionCleaner sessionCleaner;

    @Inject
    private IProjectEndpoint projectEndpoint;

    @Inject
    private ITaskEndpoint taskEndpoint;

    @Inject
    private IUserEndpoint userEndpoint;

    @Inject
    private ISessionEndpoint sessionEndpoint;

    public void start() {
        Endpoint.publish("http://localhost:8080/projectService?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8080/taskservice?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8080/userservice?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:8080/sessionservice?wsdl", sessionEndpoint);
        sessionCleaner.setDaemon(true);
        sessionCleaner.start();
    }
}
