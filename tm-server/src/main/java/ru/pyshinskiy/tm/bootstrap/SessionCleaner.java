package ru.pyshinskiy.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.service.session.ISessionService;

import javax.inject.Inject;
import java.util.Date;

import static ru.pyshinskiy.tm.constant.AppConst.SESSION_LIFE_TIME;

public class SessionCleaner extends Thread {

    @Inject
    private ISessionService sessionService;

    @Override
    public void run() {

        while(true) {
            try {
                for(@NotNull final Session session : sessionService.findAll()) {
                    if(new Date().getTime() - session.getTimestamp().getTime() > SESSION_LIFE_TIME) sessionService.remove(session.getId());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(60000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
