package ru.pyshinskiy.tm.service.user;

import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.service.IService;

public interface IUserService extends IService<User> {

    @Nullable
    User getUserByLogin(@Nullable final String login) throws Exception;
}
