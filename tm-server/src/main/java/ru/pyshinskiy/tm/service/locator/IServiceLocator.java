package ru.pyshinskiy.tm.service.locator;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.service.project.IProjectService;
import ru.pyshinskiy.tm.service.session.ISessionService;
import ru.pyshinskiy.tm.service.task.ITaskService;
import ru.pyshinskiy.tm.service.user.IUserService;

public interface IServiceLocator {

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    ISessionService getSessionService();
}
